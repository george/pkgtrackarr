## PkgTrackarr

Welcome to PkgTrackarr _(\*whistles\*)_, a project to allow for unified tracking of packages from Greek e-shops and services.

The goal of this project is to make an extensible framework that'll be able to be used to track packages all around Greece, whatever carrier (/ courier, for our Greek fellas) may be used.

## Courier support

- [x] [ELTA Courier](https://www.elta-courier.gr/)
- [x] [ACS Courier](https://www.acscourier.net)
- [ ] [Speedex](http://speedex.gr/)
- [ ] [Courier Center](https://www.courier.gr/)

- Future possibilities:
  - [ ] [ELTA (Intra-Greece Mail)](https://www.elta.gr/)
  - [ ] [Easy Mail](https://www.easymail.gr/)

**Notice: As of right now, there is no unified API. All data is passed through exactly as it is received by the courier's website, unless scraping is required.**
