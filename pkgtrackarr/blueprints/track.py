import importlib

from quart import current_app as app
from quart import Blueprint, request, jsonify
from quart.exceptions import BadRequest

bp = Blueprint("admin", __name__)


@bp.route("/track", ["GET"])
async def _get_supported_couriers():
    return jsonify({"supported_couriers": app.SUPPORTED_COURIERS})


@bp.route("/track", ["POST"])
async def _track_package():
    # TODO: validate w/ cerberus
    req = await request.json

    if req["courier"] not in app.SUPPORTED_COURIERS:
        raise BadRequest()

    mod = importlib.import_module(f".couriers.{req['courier']}", package="pkgtrackarr")
    return await mod.track(req["number"])
