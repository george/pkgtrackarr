import json
from quart import current_app as app
from quart import jsonify


async def track(number):
    querystring = {
        "p_p_id": "ACSCustomersAreaTrackTrace_WAR_ACSCustomersAreaportlet",
        "p_p_lifecycle": "2",
        "p_p_state": "normal",
        "p_p_mode": "view",
        "p_p_resource_id": "trackTraceJson",
        "p_p_cacheability": "cacheLevelPage",
        "p_p_col_id": "column-1",
        "p_p_col_count": "1",
        "_ACSCustomersAreaTrackTrace_WAR_ACSCustomersAreaportlet_jspPage": "TrackTraceController",
        "_ACSCustomersAreaTrackTrace_WAR_ACSCustomersAreaportlet_implicitModel": "true",
        "_ACSCustomersAreaTrackTrace_WAR_ACSCustomersAreaportlet_javax.portlet.action": "trackTrace",
        "_ACSCustomersAreaTrackTrace_WAR_ACSCustomersAreaportlet_generalCode": number,
    }

    resp = await app.aiohttp_session.get(
        "https://www.acscourier.net/en/track-and-trace", params=querystring
    )
    resp = await resp.text()
    resp = resp.encode().decode("utf-8-sig")

    return jsonify(json.loads(resp))
