import json

from quart import jsonify
from quart import current_app as app


async def track(number):
    resp = await app.aiohttp_session.post(
        "https://www.elta-courier.gr/track.php", data={"number": number}
    )
    resp = await resp.text()
    resp = resp.encode().decode("utf-8-sig")

    return jsonify(json.loads(resp))

