class APIError(Exception):
    """General API Error"""

    status_code = 500

    def get_payload(self):
        return {}


class BadRequest(APIError):
    """Returns HTTP code 400"""

    status_code = 400
