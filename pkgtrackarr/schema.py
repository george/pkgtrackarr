from .errors import BadRequest
from cerberus import Validator


def Validate(document, schema):
    if document is None:
        return False

    schema = Validator(schema)
    valid = schema.validate(document)

    if not valid:
        raise BadRequest(schema.errors)
    else:
        return valid
