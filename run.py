import config

import os
from os.path import dirname, basename, isfile, join
from pathlib import PosixPath, PurePosixPath, PureWindowsPath, WindowsPath

import asyncio, aiohttp, glob

from quart import Quart
from quart.json import jsonify

from pkgtrackarr.errors import APIError
from pkgtrackarr.blueprints import track

app = Quart(__name__)


@app.before_serving
async def app_before_serving():
    try:
        app.loop
    except AttributeError:
        app.loop = asyncio.get_event_loop()

    try:
        app.SUPPORTED_COURIERS = [
            basename(f)[:-3]
            for f in glob.glob(
                join(dirname(__file__), PosixPath("pkgtrackarr", "couriers", "*.py"))
            )
            if isfile(f) and not f.endswith("__init__.py")
        ]
    except:  # Windows support
        app.SUPPORTED_COURIERS = [
            basename(f)[:-3]
            for f in glob.glob(
                join(dirname(__file__), WindowsPath("pkgtrackarr", "couriers", "*.py"))
            )
            if isfile(f) and not f.endswith("__init__.py")
        ]
    app.aiohttp_session = aiohttp.ClientSession(loop=app.loop)


@app.errorhandler(APIError)
def handle_api_error(exception):
    """Handle any kind of application-level raised error."""

    scode = exception.status_code
    res = {"error": True, "code": scode, "message": exception.args[0]}

    res.update(exception.get_payload())

    res = jsonify(res)
    res.status_code = scode
    return res


def set_blueprints(app_):
    app_.register_blueprint(track.bp)


set_blueprints(app)


def main():
    """Main Application Entrypoint"""
    app.run(host=config.HOST, port=config.PORT)


if __name__ == "__main__":
    main()
